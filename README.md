## Loopback config loader
This component is designed as a configuration loader.
It looks at the projects root directory for a folder called config and processes and js/json/json5/yaml files.

The config directory structure should be as follows...
```
config
    default
        index.json
        anyname.yaml
        db
            index.json
            anothername.js
    production
        index.json
        db
            anothername.json
    development
        index.json
```

Any files with the name "index" with a supported extension will be considered a root config value based on it's path.

default/index.json will have its contents be part of the root of the config object

default/db/index.json will have its contents be part of the root of the config.db object

default/db/anothername.js will have its contents be part of config.db.anothername

production/db/anothername.json will override contents from default/db/anothername.js

Index contents get set first with additional config files overwriting content if necessary.
