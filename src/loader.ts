import glob from "glob";
import _ from "lodash";
import {resolve as resolveConfig} from "app-root-path";
import path from "path";
import fs from "fs";
import json5 from "json5";
import yaml from "yaml";

export type ENVIRONMENT = "production" | "development";

export class Loader {
    defaults: any;
    config: any;
    configDir = resolveConfig(`.${path.sep}config`);

    constructor() {
        this.init();
    }

    init() {
        const indexes = glob.sync('**/index.+(js|yaml|yml|json|json5)', {cwd: this.configDir});
        const additionalFiles = glob.sync('**/!(index).+(js|yaml|yml|json|json5)', {cwd: this.configDir});
        const defaultConfigs = this.filterConfigsFilesByEnvironment("default", indexes, additionalFiles);
        const productionConfigs = this.filterConfigsFilesByEnvironment("production", indexes, additionalFiles);
        const developmentConfigs = this.filterConfigsFilesByEnvironment("development", indexes, additionalFiles);
        if (process.env.NODE_ENV === 'production') {
            this.defaults = this.buildConfig(defaultConfigs);
            this.config = this.buildConfig(productionConfigs, this.defaults);
        }
        else {
            this.defaults = this.buildConfig(defaultConfigs);
            this.config = this.buildConfig(developmentConfigs, this.defaults);
        }
    }
    filterConfigsFilesByEnvironment(
        environment: ENVIRONMENT | "default",
        indexes: string[],
        additionalFiles: string[]
    ): string[] {
        let configs = [];
        for (const index of indexes) {
            if (index.startsWith(environment)) {
                configs.push(index);
            }
        }
        for (const additionalFile of additionalFiles) {
            if (additionalFile.startsWith(environment)) {
                configs.push(additionalFile);
            }
        }
        return configs;
    }
    buildConfig(files: string[], config: any = {}) {
        config = _.cloneDeep(config);
        for (const file of files) {
            const fullFilePath = resolveConfig(`.${path.sep}config${path.sep}${file}`);
            const ext = path.extname(fullFilePath)
            const basename = path.basename(fullFilePath, ext);
            let configPath = file.substring(0, file.length - ext.length).split('/');
            if (basename === 'index') {
                if (configPath.length === 2) {
                    if (ext === '.js') {
                        config = _.defaultsDeep(require(fullFilePath), config);
                    }
                    else if (ext === '.json') {
                        config = _.defaultsDeep(JSON.parse(fs.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    else if (ext === '.json5') {
                        config = _.defaultsDeep(json5.parse(fs.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    else if (ext === '.yaml' || ext === '.yml') {
                        config = _.defaultsDeep(yaml.parse(fs.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    continue;
                }
                else {
                    configPath.pop();
                }
            }
            configPath.shift();
            if (_.has(config, configPath.join('.'))) {
                if (ext === '.js') {
                    _.set(
                        config,
                        configPath.join('.'),
                        _.defaultsDeep(
                            require(fullFilePath),
                            _.get(config, configPath.join('.'))
                        )
                    );
                }
                else if (ext === '.json') {
                    _.set(
                        config,
                        configPath.join('.'),
                        _.defaultsDeep(
                            JSON.parse(fs.readFileSync(fullFilePath, 'utf8')),
                            _.get(config, configPath.join('.'))
                        )
                    );
                }
                else if (ext === '.json5') {
                    _.set(
                        config,
                        configPath.join('.'),
                        _.defaultsDeep(
                            json5.parse(fs.readFileSync(fullFilePath, 'utf8')),
                            _.get(config, configPath.join('.'))
                        )
                    );
                }
                else if (ext === '.yaml' || ext === '.yml') {
                    _.set(
                        config,
                        configPath.join('.'),
                        _.defaultsDeep(
                            yaml.parse(fs.readFileSync(fullFilePath, 'utf8')),
                            _.get(config, configPath.join('.'))
                        )
                    );
                }
            }
            else {
                if (ext === '.js') {
                    _.set(config, configPath.join('.'), require(fullFilePath));
                }
                else if (ext === '.json') {
                    _.set(config, configPath.join('.'), JSON.parse(fs.readFileSync(fullFilePath, 'utf8')));
                }
                else if (ext === '.json5') {
                    _.set(config, configPath.join('.'), json5.parse(fs.readFileSync(fullFilePath, 'utf8')));
                }
                else if (ext === '.yaml' || ext === '.yml') {
                    _.set(config, configPath.join('.'), yaml.parse(fs.readFileSync(fullFilePath, 'utf8')));
                }
            }
        }
        return config;
    }
}
