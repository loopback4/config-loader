import { BindingKey } from "@loopback/core";
import {ConfigProvider} from "./configProvider";

export namespace ConfigLoaderBindings {
    export const CONFIG_LOADER = BindingKey.create<ConfigProvider>('services.config-loader.loader')
}
