import {WebsocketApplication} from "@loopback4/websocket";
import { Component, CoreBindings, inject, BindingKey, Binding } from '@loopback/core';
import { ConfigProvider } from './configProvider';
import {ConfigLoaderBindings} from "./keys";
export * from './keys';

export class ConfigLoaderComponent implements Component {
    // providers = {
    //     'config-loader.config': ConfigProvider
    // };
    bindings = [
        Binding.bind(ConfigLoaderBindings.CONFIG_LOADER).toClass(ConfigProvider),
    ];
    constructor(
        @inject(CoreBindings.APPLICATION_INSTANCE) private application: WebsocketApplication,
    ) {
    }
}
