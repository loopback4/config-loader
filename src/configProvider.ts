import { Provider } from '@loopback/context';
import {BindingScope, injectable } from '@loopback/core';
import { Loader } from './loader';

@injectable({scope: BindingScope.SINGLETON})
export class ConfigProvider extends Loader implements Provider<object> {
    constructor() {
        super();
    }

    value() {
        return this.config;
    }


}
