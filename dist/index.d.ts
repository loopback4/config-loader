import { WebsocketApplication } from "@loopback4/websocket";
import { Component, Binding } from '@loopback/core';
import { ConfigProvider } from './configProvider';
export * from './keys';
export declare class ConfigLoaderComponent implements Component {
    private application;
    bindings: Binding<ConfigProvider>[];
    constructor(application: WebsocketApplication);
}
