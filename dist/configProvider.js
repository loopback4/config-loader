"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigProvider = void 0;
const tslib_1 = require("tslib");
const core_1 = require("@loopback/core");
const loader_1 = require("./loader");
let ConfigProvider = class ConfigProvider extends loader_1.Loader {
    constructor() {
        super();
    }
    value() {
        return this.config;
    }
};
ConfigProvider = tslib_1.__decorate([
    (0, core_1.injectable)({ scope: core_1.BindingScope.SINGLETON }),
    tslib_1.__metadata("design:paramtypes", [])
], ConfigProvider);
exports.ConfigProvider = ConfigProvider;
//# sourceMappingURL=configProvider.js.map