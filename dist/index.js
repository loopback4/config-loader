"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigLoaderComponent = void 0;
const tslib_1 = require("tslib");
const websocket_1 = require("@loopback4/websocket");
const core_1 = require("@loopback/core");
const configProvider_1 = require("./configProvider");
const keys_1 = require("./keys");
tslib_1.__exportStar(require("./keys"), exports);
let ConfigLoaderComponent = class ConfigLoaderComponent {
    constructor(application) {
        this.application = application;
        // providers = {
        //     'config-loader.config': ConfigProvider
        // };
        this.bindings = [
            core_1.Binding.bind(keys_1.ConfigLoaderBindings.CONFIG_LOADER).toClass(configProvider_1.ConfigProvider),
        ];
    }
};
ConfigLoaderComponent = tslib_1.__decorate([
    tslib_1.__param(0, (0, core_1.inject)(core_1.CoreBindings.APPLICATION_INSTANCE)),
    tslib_1.__metadata("design:paramtypes", [websocket_1.WebsocketApplication])
], ConfigLoaderComponent);
exports.ConfigLoaderComponent = ConfigLoaderComponent;
//# sourceMappingURL=index.js.map