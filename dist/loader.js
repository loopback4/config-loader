"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Loader = void 0;
const tslib_1 = require("tslib");
const glob_1 = tslib_1.__importDefault(require("glob"));
const lodash_1 = tslib_1.__importDefault(require("lodash"));
const app_root_path_1 = require("app-root-path");
const path_1 = tslib_1.__importDefault(require("path"));
const fs_1 = tslib_1.__importDefault(require("fs"));
const json5_1 = tslib_1.__importDefault(require("json5"));
const yaml_1 = tslib_1.__importDefault(require("yaml"));
class Loader {
    constructor() {
        this.configDir = (0, app_root_path_1.resolve)(`.${path_1.default.sep}config`);
        this.init();
    }
    init() {
        const indexes = glob_1.default.sync('**/index.+(js|yaml|yml|json|json5)', { cwd: this.configDir });
        const additionalFiles = glob_1.default.sync('**/!(index).+(js|yaml|yml|json|json5)', { cwd: this.configDir });
        const defaultConfigs = this.filterConfigsFilesByEnvironment("default", indexes, additionalFiles);
        const productionConfigs = this.filterConfigsFilesByEnvironment("production", indexes, additionalFiles);
        const developmentConfigs = this.filterConfigsFilesByEnvironment("development", indexes, additionalFiles);
        if (process.env.NODE_ENV === 'production') {
            this.defaults = this.buildConfig(defaultConfigs);
            this.config = this.buildConfig(productionConfigs, this.defaults);
        }
        else {
            this.defaults = this.buildConfig(defaultConfigs);
            this.config = this.buildConfig(developmentConfigs, this.defaults);
        }
    }
    filterConfigsFilesByEnvironment(environment, indexes, additionalFiles) {
        let configs = [];
        for (const index of indexes) {
            if (index.startsWith(environment)) {
                configs.push(index);
            }
        }
        for (const additionalFile of additionalFiles) {
            if (additionalFile.startsWith(environment)) {
                configs.push(additionalFile);
            }
        }
        return configs;
    }
    buildConfig(files, config = {}) {
        config = lodash_1.default.cloneDeep(config);
        for (const file of files) {
            const fullFilePath = (0, app_root_path_1.resolve)(`.${path_1.default.sep}config${path_1.default.sep}${file}`);
            const ext = path_1.default.extname(fullFilePath);
            const basename = path_1.default.basename(fullFilePath, ext);
            let configPath = file.substring(0, file.length - ext.length).split('/');
            if (basename === 'index') {
                if (configPath.length === 2) {
                    if (ext === '.js') {
                        config = lodash_1.default.defaultsDeep(require(fullFilePath), config);
                    }
                    else if (ext === '.json') {
                        config = lodash_1.default.defaultsDeep(JSON.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    else if (ext === '.json5') {
                        config = lodash_1.default.defaultsDeep(json5_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    else if (ext === '.yaml' || ext === '.yml') {
                        config = lodash_1.default.defaultsDeep(yaml_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), config);
                    }
                    continue;
                }
                else {
                    configPath.pop();
                }
            }
            configPath.shift();
            if (lodash_1.default.has(config, configPath.join('.'))) {
                if (ext === '.js') {
                    lodash_1.default.set(config, configPath.join('.'), lodash_1.default.defaultsDeep(require(fullFilePath), lodash_1.default.get(config, configPath.join('.'))));
                }
                else if (ext === '.json') {
                    lodash_1.default.set(config, configPath.join('.'), lodash_1.default.defaultsDeep(JSON.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), lodash_1.default.get(config, configPath.join('.'))));
                }
                else if (ext === '.json5') {
                    lodash_1.default.set(config, configPath.join('.'), lodash_1.default.defaultsDeep(json5_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), lodash_1.default.get(config, configPath.join('.'))));
                }
                else if (ext === '.yaml' || ext === '.yml') {
                    lodash_1.default.set(config, configPath.join('.'), lodash_1.default.defaultsDeep(yaml_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')), lodash_1.default.get(config, configPath.join('.'))));
                }
            }
            else {
                if (ext === '.js') {
                    lodash_1.default.set(config, configPath.join('.'), require(fullFilePath));
                }
                else if (ext === '.json') {
                    lodash_1.default.set(config, configPath.join('.'), JSON.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')));
                }
                else if (ext === '.json5') {
                    lodash_1.default.set(config, configPath.join('.'), json5_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')));
                }
                else if (ext === '.yaml' || ext === '.yml') {
                    lodash_1.default.set(config, configPath.join('.'), yaml_1.default.parse(fs_1.default.readFileSync(fullFilePath, 'utf8')));
                }
            }
        }
        return config;
    }
}
exports.Loader = Loader;
//# sourceMappingURL=loader.js.map