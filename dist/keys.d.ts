import { BindingKey } from "@loopback/core";
import { ConfigProvider } from "./configProvider";
export declare namespace ConfigLoaderBindings {
    const CONFIG_LOADER: BindingKey<ConfigProvider>;
}
