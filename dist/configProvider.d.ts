import { Provider } from '@loopback/context';
import { Loader } from './loader';
export declare class ConfigProvider extends Loader implements Provider<object> {
    constructor();
    value(): any;
}
