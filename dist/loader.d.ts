export declare type ENVIRONMENT = "production" | "development";
export declare class Loader {
    defaults: any;
    config: any;
    configDir: string;
    constructor();
    init(): void;
    filterConfigsFilesByEnvironment(environment: ENVIRONMENT | "default", indexes: string[], additionalFiles: string[]): string[];
    buildConfig(files: string[], config?: any): any;
}
