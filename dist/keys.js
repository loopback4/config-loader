"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigLoaderBindings = void 0;
const core_1 = require("@loopback/core");
var ConfigLoaderBindings;
(function (ConfigLoaderBindings) {
    ConfigLoaderBindings.CONFIG_LOADER = core_1.BindingKey.create('services.config-loader.loader');
})(ConfigLoaderBindings = exports.ConfigLoaderBindings || (exports.ConfigLoaderBindings = {}));
//# sourceMappingURL=keys.js.map